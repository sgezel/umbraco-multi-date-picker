# RB: Multi Date Picker

The Multi Date Picker package is an Umbraco data type for creating a collection of dates within Umbraco. An editor can add, edit and delete dates within a property on a page. This package reuses Umbraco's built in date picker.

## Installation

The Multi Date Picker package can be installed via the package's page on [our.umbraco.org](http://our.umbraco.org/projects/backoffice-extensions/multi-date-picker) or via NuGet. If installing via NuGet, use the following package manager command:

    Install-Package RB.MultiDatePicker

## Configuration

Setup a new data type and select the RB.MultiDatePicker within Umbraco's Developer section. Once setup, you will be able to add the data type as a property to a document type. When an instance of that document type is created, the editor will be displayed with a collection of editable dates.

## Usage

When using a property value on a template, add the following code to return and use a list of dates.

    var dates = Model.Content.GetPropertyValue<List<DateTime>>("alias");

Once converted, you will be able to select or loop through each date. For example:

    @{
        <ul>
            @foreach (var date in dates)
            {
                <li>@date.ToUniversalTime()</li>
            }
        </ul>
    }

## Contributing

To raise a new bug, create an [issue](https://bitbucket.org/rbdigital/umbraco-multi-date-picker/issues) on the Bitbucket repository. To fix a bug or add new features or providers, fork the repository and send a [pull request](https://bitbucket.org/rbdigital/umbraco-multi-date-picker/pull-requests) with your changes. Feel free to add ideas to the repository's [issues](https://bitbucket.org/rbdigital/umbraco-multi-date-picker/issues) list if you would to discuss anything related to the package.

## Publishing

Remember to include all necessary files within the package.xml file. Run the following script, entering the new version number when prompted to created a published version of the package:

    Build\Release.bat

The release script will amend all assembly versions for the package, build the solution and create the package file. The script will also commit and tag the repository accordingly to reflect the new version.