﻿angular.module("umbraco").controller("RB.MultiDatePicker.Dialog", function ($scope) {

    if (typeof $scope.dialogData === 'object')
        $scope.dialogData = null;

    $scope.datepicker = {
        view: 'datepicker',
        config: {
            pickDate: true,
            pickTime: false,
            pick12HourFormat: false,
            format: "yyyy-MM-dd"

        },
        value: $scope.dialogData
    };

});